package praktikum2;

import naiteYlesanded.TextIO;

public class TekstiAsendus {
	
	public static void main(String[] args) {
		
		System.out.println("Palun sisesta tekst.");
		
		String tekst = TextIO.getlnString();
		
		System.out.println(tekst.replace("a", "-"));
	}
	
}
