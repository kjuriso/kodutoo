package praktikum3;

public class ParooliKysimine {
	public static void main(String[] args) {

		int sisestusteArv = 0;
		while (true) {

			String parool = "saladus"; // pole lihtsalt muutuja, vaid objekt.
			// sellep�rast ka String suure t�hega -
			// n�itab klassi

			System.out.println("Sisesta parool");
			String kasutajaSisestus = TextIO.getlnString();

			if (parool.equals(kasutajaSisestus)) {
				System.out.println("�ige parool");
				break;
			} else {
				System.out.println("Vale parool!");
				sisestusteArv = sisestusteArv + 1; // v�ib kirjutada ka ++
			}
			if (sisestusteArv >= 3) {
				System.out.println("�le kolme korra ei saa proovida");
				break;

			}
		}

	}

}
