package omaLahendused;

public class KeskmisestVaiksemad {

	


   public static void main (String[] args) {
      System.out.println (allaKeskmise (new double[]{0., 1., 2., 3., 4.}));
      // YOUR TESTS HERE
   }

   public static int allaKeskmise (double[] d) {
	   double summa = 0;
	   double keskmine = 0;
	   int count = 0;
	   
	   for (int i = 0; i < d.length; i++) {
		summa = summa + d[i];
	}
	   keskmine = summa / d.length;
	   
	   for (int i = 0; i < d.length; i++) {
		if (d[i] < keskmine){
			count = count + 1;
		}
	}
      return count;  // YOUR PROGRAM HERE
   }

}
