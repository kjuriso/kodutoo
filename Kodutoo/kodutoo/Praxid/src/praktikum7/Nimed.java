package praktikum7;

import praktikum2.TextIO;
import java.util.ArrayList;


public class Nimed {

	public static void main(String[] args) {

		ArrayList<String> nimed = new ArrayList<String>();
		String nimi;
		do {
			System.out.println("Palun sisesta nimed, l�petamiseks sisesta t�hi rida.");
			nimi = TextIO.getln();

			nimed.add(nimi);
		} while (nimi.length() != 0);
		
		for (String n : nimed) {
		    System.out.println(n);
		}
	}
}
