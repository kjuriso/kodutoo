package praktikum3;

public class CumLaude {

	public static void main(String[] args) {

		double keskmineHinne;
		while (true) {
			System.out.println("Palun sisesta keskmine hinne.");
			keskmineHinne = TextIO.getlnDouble();
			if (keskmineHinne < 0 || keskmineHinne > 5) {
				System.out.println("Vigane hinne, proovi uuesti");
			} else {
				break;
			}
		}

		System.out.println("Palun sisesta l�put�� hinne.");
		int l6put66 = TextIO.getlnInt();

		if (l6put66 < 0 || l6put66 > 5) {
			System.out.println("Vigane hinne!");
			return;
		}

		// && -- loogiline JA
		// || -- loogiline VÕI

		if (keskmineHinne > 4.5 && 5 == l6put66) {
			System.out.println("Jah, saad Cum Laude!");
		} else {
			System.out.println("Ei saa.");
		}
	}
}

// proovi panna while'ga t��le nii, et kui sisestab vigase hinde, siis saab
// uuesti sisestada, mitte ei l�peta programmi t��d'