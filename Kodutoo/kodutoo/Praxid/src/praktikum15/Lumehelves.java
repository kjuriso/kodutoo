package praktikum15;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Lumehelves {
	int x, y;

	public Lumehelves(int x, int y) {
		this.x = x;
		this.y = y;

	}

	public void joonistaMind(GraphicsContext gc) {
		gc.setStroke(Color.BLUE);
		gc.strokeLine(x - 10, y - 10, x + 10, y + 10);
		gc.strokeLine(x - 10, y + 10, x + 10, y - 10);
		gc.strokeLine(x, y - 10, x, y + 10);
		gc.strokeLine(x - 10, y, x + 10, y);
	}

}
