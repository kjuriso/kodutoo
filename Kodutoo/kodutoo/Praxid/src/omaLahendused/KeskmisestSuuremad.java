package omaLahendused;

public class KeskmisestSuuremad {

	public static void main(String[] args) {
		

		System.out.println(keskmisestParemaid(new double[] { 0., 1., 3., 2., 4. }));

	}

	public static int keskmisestParemaid(double[] d) {
		double summa = 0;
		double keskmine = 0;
		int count = 0;

		for (int i = 0; i < d.length; i++) {
			summa = summa + d[i];

		}
		keskmine = summa / d.length;

		for (int i = 0; i < d.length; i++) {
			if (d[i] > keskmine) {
				count = count + 1;
			}
		}

		return count;
	}

}