package praktikum9;

public class MaatriksiSuurim {

	public static void main(String[] args) {

		int[][] neo = { { 1000, 3, 6, 79 }, { 2, 3, 3333, 1 }, { 17, 444, 5, 0 }, { -20, 13, 16, 5 } };
		System.out.println(max(neo));
	}

	public static int max(int[][] sisend) {

		int maks = 0;

		for (int[] el : sisend) {
			int vaartus = MassiiviSuurim.suurim(el);
			if (vaartus > maks) {
				maks = vaartus;
			}
		}
	return maks;
	}
}
