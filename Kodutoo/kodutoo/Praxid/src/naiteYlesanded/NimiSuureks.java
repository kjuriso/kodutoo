package naiteYlesanded;

public class NimiSuureks {
	
	public static void main(String[] args) {
		
		System.out.println("What is your name?");
		String nimi = TextIO.getlnString();
		
		System.out.println("Hello, " + nimi.toUpperCase() + ", nice to meet you!");
	}

}
