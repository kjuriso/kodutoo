package naiteYlesanded;

import java.util.Arrays;

public class ReaMinid {

	public static void main(String[] args) {

		int[] res = reaMin(new int[][] { { 6, 5, 3, 1 }, { 7, 4, 3, 0, 1 }, { 3, 2 } });
		System.out.println(Arrays.toString(res));

	}

	private static int[] reaMin(int[][] m) {

		int[] min = new int[m.length];

		for (int i = 0; i < m.length; i++) {
			min[i] = m[i][0];

			for (int j = 0; j < m[i].length; j++) {
				if (min[i] > m[i][j]) {
					min[i] = m[i][j];
				}

			}

		}
		return min;
	}
	
	// byte - 8-bitine t�isarv vahemikus -128-127
	// short - 16-bitine t�isarv vahemikus -32768-32767
	// int - 32-bitine t�isarv vahemikus -2147483648-
	// n�it: 0x84ed1
	// long - 64-bitine t�isarv
	// n�it: 12L, -0xfcd45dL
	// float - 32-bitine ujupunktarv
	// n�it: -5,4F, 32e+17F, 0,5e-6F
	// double - 64-bitine ujupunktarv
	// n�it: 8.3.-5e-100
	// char - 16-bitine Unicode s�mbol
}