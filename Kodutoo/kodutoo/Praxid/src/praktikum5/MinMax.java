package praktikum5;

public class MinMax {

	public static void main(String[] args) {

		int kasutajaSisestas = kasutajaSisestus(1, 10);
		System.out.println("Tubli, arv " + kasutajaSisestas + " j��b sellesse vahemikku.");
	}

	public static int kasutajaSisestus(int min, int max) {
		while (true) {

			System.out.println("Palun sisesta �ks arv vahemikus " + min + " kuni " + max);
			int sisestus = TextIO.getInt();
			if (sisestus >= min && sisestus <= max) {
				return sisestus;
			} else {
				System.out.println("See arv ei j�� lubatud vahemikku.");
			}
		}
	}

}
