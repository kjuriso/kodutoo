package praktikum5;

import praktikum2.TextIO;

public class Meetod {

	// main meetod on see, kust kõik Java programmid alustavad tööd
	public static void main(String[] args) {

		System.out.println(Meetod.minuMeetod(3, "Tere veel."));
		//int arv = TextIO.getlnInt();
		System.out.println(Math.abs(4));
	}

	public static String minuMeetod(int mituKorda, String lisaTekst) {
		String tagastus = " ";
		for (int i = 0; i < mituKorda;  i++) {
			tagastus += "See tekst on p�rit minuMeetod-i seest.\n";
		}
	return tagastus + lisaTekst;
	}
	
	public static int kasutajaSisestus(int min, int max) {

        int sisestus;
        do {
            System.out.println("Palun sisesta number vahemikus " + min + " kuni " + max);
            sisestus = TextIO.getlnInt();           
        } while (sisestus < min || sisestus > max);
        return sisestus;
    }
}
