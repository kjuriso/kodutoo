package mang;

import javax.swing.JPanel;
import javax.swing.Timer;

import manguElemendid.Lapats;
import manguElemendid.Pall;
import manguElemendid.Tellis;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import manguElemendid.Tellis.Type;

public class ManguPaneel extends JPanel implements ActionListener, KeyListener {

	/**
	 * Minu esimene Java m�ng: Breakout
	 * 
	 * @autor: Katrin J�riso
	 * 
	 *         kasutatud Breakout tutoriali abi
	 *         http://www.java-forums.org/blogs/sunde887/93-breakout-tutorial.
	 *         html
	 */

	/** default serial version */

	private static final long serialVersionUID = 1L;

	/** loogilist t��pi muutujate deklareerimine */
	private boolean tiitelleht = true;
	private boolean start = false;
	private boolean mangLabi = false;
	private boolean vasakule = false;
	private boolean paremale = false;

	/** uue lapatsi ja palli loomine */
	Lapats lapats = new Lapats(350, 550, 200, 20, 8);
	Pall pall = new Pall(390, 530, 20);

	/** uue ArrayListi <Tellised> loomine */
	ArrayList<ArrayList<Tellis>> tellised;

	/** meetodi "ManguPaneel" loomine */
	public ManguPaneel() {

		/** meetodi "ManguPaneel" v�ljakutsumisega kaasneb telliste loomine */
		looTellised();

		/** uue v�rvi loomine ja uue v�rvi taustav�rviks m��ramine */
		Color minuUusSinine = new Color(112, 141, 174);
		setBackground(minuUusSinine);

		/** meetodi fokusseerimisv�ime algatamine */
		setFocusable(true);

		/** klahvivajutuse j�lgimine */
		addKeyListener(this);

		/** timeri kalibreerimine */
		Timer timer = new Timer(15, this);
		timer.start();
	}

	/** m�nguelementide joonistamise meetod */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		/** tiitellehe joonistamine */
		if (tiitelleht) {
			Color minuUusKollane = new Color(255, 255, 0);
			g.setColor(minuUusKollane);
			g.setFont(new Font(Font.DIALOG, Font.BOLD, 55));
			g.drawString("Vana hea BREAKOUT!", 120, 200);
			g.setColor(Color.white);
			g.setFont(new Font(Font.DIALOG, Font.BOLD, 40));
			g.drawString("Tere tulemast!", 270, 270);
			g.setColor(minuUusKollane);
			g.setFont(new Font(Font.DIALOG, Font.BOLD, 24));
			g.drawString("Palli p��dmiseks kasuta klahve PAREMALE ja VASAKULE.", 65, 380);
			g.drawString("M�ngu alustamiseks vajuta T�HIKULE.", 170, 440);
			g.setColor(Color.white);
			g.setFont(new Font(Font.DIALOG, Font.BOLD, 32));
			g.drawString("Head m�ngu!", 320, 4700);
		}

		/** t��tava m�nguosa joonistamine */
		if (start) {
			g.setColor(Color.gray);
			g.fillRect(0, 0, 800, 600);
			lapats.joonista(g);
			pall.joonista(g);
			for (ArrayList<Tellis> rida : tellised) {
				for (Tellis t : rida) {
					t.joonistaTellis(g);
				}
			}
			g.setColor(lapats.varv);
			g.setFont(new Font(Font.DIALOG, Font.BOLD, 28));
			g.drawString("Skoor: " + lapats.tooSkoor(), 560, 35);
			g.setFont(new Font(Font.DIALOG, Font.BOLD, 18));
			g.drawString("Roheline = 20p, Sinine = 50p, Punane = 100p", 10, 35);
		}
		/** siin valmib leht, mis kuvatakse p�rast m�ngu l�bisaamist */
		if (mangLabi) {
			Color minuUusKollane = new Color(255, 255, 0);
			g.setColor(minuUusKollane);
			g.fillRect(0, 0, 800, 600);
			g.setColor(Color.black);
			g.setFont(new Font(Font.DIALOG, Font.BOLD, 54));
			g.drawString("M�ng l�bi!", 250, 170);
			g.setFont(new Font(Font.DIALOG, Font.BOLD, 30));
			g.drawString("Sa kogusid:", 290, 240);
			Color minuUusRoheline = new Color(0, 153, 0);
			g.setColor(minuUusRoheline);
			g.setFont(new Font(Font.DIALOG, Font.BOLD, 46));
			g.drawString(" " + lapats.tooSkoor() + " punkti!", 250, 320);
			g.setColor(Color.black);
			g.setFont(new Font(Font.DIALOG, Font.BOLD, 30));
			if (lapats.tooSkoor() < 300) {
				g.drawString("P�ris hea tulemus, aga alati saab paremini;)", 100, 400);
			} else if (lapats.tooSkoor() >= 300 && lapats.tooSkoor() < 1500) {
				g.drawString("Sa oled ju p�ris k�va k�si!", 230, 400);
			} else {
				g.drawString("Sa oled uskumatult tubli!", 240, 420);
			}
			g.setFont(new Font(Font.DIALOG, Font.BOLD, 24));
			g.setColor(Color.black);
			g.drawString("Uuesti alustamiseks vajuta T�HIKULE.", 140, 480);
		}
	}

	/**
	 * meetod, mis loob tellised 
	 * meetodi loomisel kasutatud Breakout tutoriali
	 * abi http://www.java-forums.org/blogs/sunde887/93-breakout-tutorial.html
	 */

	public void looTellised() {
		tellised = new ArrayList<ArrayList<Tellis>>();

		/**
		 * k�igepealt luuakse uus ajutine telliste arraylist ning ridade kaupa
		 * muudetakse telliste t��pe
		 */
		for (int i = 0; i < 10; ++i) {
			ArrayList<Tellis> temp = new ArrayList<Tellis>();
			Type reaVarv = null;
			switch (i) {
			case 0:
			case 7:
			case 3:
				reaVarv = Type.III;
				break;
			case 4:
			case 8:
				reaVarv = Type.II;
				break;
			default:
				reaVarv = Type.I;
				break;
			}

			/**
			 * veerude kaupa luuakse uued ajutised tellised, mis lisatakse
			 * ajutisele telliste arraylistile
			 */
			for (int j = 0; j < 10; ++j) {

				Tellis tempTellis = new Tellis(((j * Tellis.tellis_laius)), (((i + 2) * Tellis.tellis_korgus)),
						reaVarv);

				temp.add(tempTellis);

			}
			tellised.add(temp);
		}
	}

	/**
	 * meetod, mis deklareerib, mis juhtub tegevuse initsialiseerimise korral
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		kontrolliKokkuPorkeid();
		step();
	}

	/**
	 * meetod, mis kirjeldab, mis hakkab tegevuse initsialiseerimise korral nn
	 * sammhaaval juhtuma
	 */
	private void step() {

		if (start) {

			/** palli liigutamise meetodi esilekutsumine */
			pallLiigu();

			/** lapatsi vasakule liigutamise meetodi esilekutsumine */
			if (vasakule) {
				lapatsiLiikumineVasakule();
			}
			/** lapatsi paremale liigutamise meetodi esilekutsumine */
			if (paremale) {
				lapatsiLiikumineParemale();
			}
			/**
			 * "surnud" telliste eemaldamine arraylistist luubi loomisel
			 * kasutatud Breakout tutoriali abi
			 * http://www.java-forums.org/blogs/sunde887/93-breakout-tutorial.
			 * html
			 */
			for (int i = 0; i < tellised.size(); ++i) {
				ArrayList<Tellis> t = tellised.get(i);
				for (int j = 0; j < t.size(); ++j) {
					Tellis tellis = t.get(j);
					if (tellis.dead()) {
						t.remove(tellis);
					}
				}
			}
			/** kui pall kukub raami alumiselt ��relt alla, siis saab m�ng l�bi */
			if (pall.tooPalliY() > 600) {
				start = false;
				mangLabi = true;
			}

			/** kui telliste arraylist on t�hi, siis on m�ng l�bi */
			if (tyhi()) {
				mangLabi = true;
				start = false;
			}
		}
		/** m�ngu osad on muutunud, uuesti joonistamise v�ljakutsumine */
		repaint();

	}

	/** palli liigutamise meetod */
	private void pallLiigu() {
		pall.liigu();
	}

	/** lapatsi paremale liigutamise meetod */
	private void lapatsiLiikumineParemale() {
		lapats.lapatsLiiguParemale();
	}

	/** lapatsi vasakule liigutamise meetod */
	private void lapatsiLiikumineVasakule() {
		lapats.lapatsLiiguVasakule();
	}

	/** meetod, mis kontrollib, kas m�ngu eri elemendid on p�rkunud */
	private void kontrolliKokkuPorkeid() {

		if (lapats.kokkuPorgeLapatsiga(pall)) {

			if (pall.tooPalliX() < (lapats.tooLapatsiX() + 30)) {
				pall.seaPalliXmuut(-4);
				pall.seaPalliYmuut(pall.tooPalliYmuut() * -1);
			} else if (pall.tooPalliX() <= lapats.tooLapatsiX() + 60 && pall.tooPalliX() > lapats.tooLapatsiX() + 30) {
				pall.seaPalliXmuut(-3);
				pall.seaPalliYmuut(pall.tooPalliYmuut() * -1);
			} else if (pall.tooPalliX() <= lapats.tooLapatsiX() + 90 && pall.tooPalliX() > lapats.tooLapatsiX() + 60) {
				pall.seaPalliXmuut(-2);
				pall.seaPalliYmuut(pall.tooPalliYmuut() * -1);
			} else if (pall.tooPalliX() <= lapats.tooLapatsiX() + 100 && pall.tooPalliX() > lapats.tooLapatsiX() + 90) {
				pall.seaPalliXmuut(-1);
				pall.seaPalliYmuut(pall.tooPalliYmuut() * -1);
			} else
				if (pall.tooPalliX() <= lapats.tooLapatsiX() + 110 && pall.tooPalliX() > lapats.tooLapatsiX() + 100) {
				pall.seaPalliXmuut(1);
				pall.seaPalliYmuut(pall.tooPalliYmuut() * -1);

			} else if (pall.tooPalliX() <= lapats.tooLapatsiX() + 140
					&& pall.tooPalliX() > lapats.tooLapatsiX() + 100) {
				pall.seaPalliXmuut(2);
				pall.seaPalliYmuut(pall.tooPalliYmuut() * -1);
			} else
				if (pall.tooPalliX() <= lapats.tooLapatsiX() + 170 && pall.tooPalliX() > lapats.tooLapatsiX() + 140) {
				pall.seaPalliXmuut(3);
				pall.seaPalliYmuut(pall.tooPalliYmuut() * -1);
			} else {
				pall.seaPalliXmuut(4);
				pall.seaPalliYmuut(pall.tooPalliYmuut() * -1);
			}
			return;
		}

		/**
		 * tellise ja palli kokkup�rkel lisatakse skoorile juurde tellise
		 * t��bile vastavad punktid ja kutsutakse v�lja meetod alamtyyp, mis
		 * otsustab tellise saatuse loopi loomisel kasutatud Breakout tutoriali
		 * abi
		 * http://www.java-forums.org/blogs/sunde887/93-breakout-tutorial.html
		 */
		for (int i = 0; i < tellised.size(); ++i) {
			for (Tellis t : tellised.get(i)) {
				if (t.kokkuPorge(pall)) {
					lapats.seaSkoor(lapats.tooSkoor() + t.tooTelliseTyyp().tooPunktid());
					t.alamTyyp();

				}
			}
		}
	}

	/**
	 * meetod, mis kontrollib, kas telliste arraylistis on veel elemente meetodi
	 * loomisel kasutatud Breakout tutoriali abi
	 * http://www.java-forums.org/blogs/sunde887/93-breakout-tutorial.html
	 */
	private boolean tyhi() {
		for (ArrayList<Tellis> t : tellised) {
			if (t.size() != 0) {
				return false;
			}
		}
		return true;
	}

	/** klahvivajutuse m�istmise meetod */
	public void keyTyped(KeyEvent e) {
	}

	/**
	 * meetod, mis loeb klahvivajutusi ja annab neile vastavad v��rtused ehk
	 * kirjeldab, mis juhtub mingi klahvivajutuse korral
	 */
	public void keyPressed(KeyEvent e) {

		if (tiitelleht) {
			if (e.getKeyCode() == KeyEvent.VK_SPACE) {
				start = true;
				tiitelleht = false;
				mangLabi = false;

			}
		} else if (start) {

			if (e.getKeyCode() == KeyEvent.VK_LEFT) {
				vasakule = true;

			} else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
				paremale = true;
			}
		} else if (mangLabi) {

			if (e.getKeyCode() == KeyEvent.VK_SPACE) {

				start = true;
				mangLabi = false;
				pall.seaPalliX(390);
				pall.seaPalliY(530);
				lapats.seaLapatsiX(350);
				looTellised();
				lapats.seaSkoor(0);
			}
		}
	}

	/**
	 * meetod, mis paneb paika selle, mis juhtub siis, kui klahvi enam ei
	 * vajutata ehk et v�imaldab n�iteks liikumise l�petada
	 */
	@Override
	public void keyReleased(KeyEvent e) {

		if (start) {

			if (e.getKeyCode() == KeyEvent.VK_LEFT) {
				vasakule = false;
			} else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
				paremale = false;
			}
		}
		if (mangLabi) {
			if (e.getKeyCode() == KeyEvent.VK_LEFT) {
				vasakule = false;
			} else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
				paremale = false;
			}

		}
	}
}