package mang;

import java.awt.BorderLayout;

import javax.swing.JFrame;

public class Main {

		/**peameetod (static means that the method is associated 
		 * with the class, not a specific instance (object) 
		 * of that class. This means that you can call 
		 * a static method without creating an object of the class.)*/
	public static void main(String[] args) {
		
		/**m�nguraami joonistamine, m�ngupaneeli lisamine, suuruse m��ramine ja
		 * raami suuruse fikseerimine*/
		JFrame raam = new JFrame("Minu esimene Java m�ng");
		raam.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		raam.setLayout(new BorderLayout());
		ManguPaneel manguPaneel = new ManguPaneel();
		raam.add(manguPaneel, BorderLayout.CENTER);
		raam.setVisible(true);
		raam.setSize(800, 600);
		raam.setResizable(false);
	}
}
