package manguElemendid;

import java.awt.Color;
import java.awt.Graphics;

/** palli klass */
public class Pall {

	/** palli klassi muutujate kirjeldamine */
	public Color varv;
	private int palliLabiMoot, palliX, palliY;
	private int palliXmuut = 2;
	private int palliYmuut = -4;

	/**
	 * meetodid, mis tagastavad muutuja v��rtuse v�i muudavad muutuja v��rtust
	 * kasutatud Breakout tutoriali abi
	 * http://www.java-forums.org/blogs/sunde887/93-breakout-tutorial.html
	 */
	public void seaPalliYmuut(int palliYmuut) {
		this.palliYmuut = palliYmuut;
	}

	public int tooPalliYmuut() {
		return palliYmuut;
	}

	public void seaPalliXmuut(int palliXmuut) {
		this.palliXmuut = palliXmuut;
	}

	public int tooPalliXmuut() {
		return palliXmuut;
	}

	public int tooPalliX() {
		return palliX;
	}

	public int tooPalliY() {
		return palliY;
	}

	public void seaPalliX(int palliX) {
		this.palliX = palliX;
	}

	public void seaPalliY(int palliY) {
		this.palliY = palliY;
	}

	/** palli loomise konstruktor */
	public Pall(int PalliX, int PalliY, int PalliLabiMoot) {
		palliX = PalliX;
		palliY = PalliY;
		palliLabiMoot = PalliLabiMoot;
	}

	/** palli joonistamise meetod */
	public void joonista(Graphics g) {

		g.setColor(Color.yellow);
		g.fillOval(palliX, palliY, palliLabiMoot, palliLabiMoot);
	}

	/** meetod, mis defineerib, kuidas pall liigub */
	public void liigu() {
		if (palliX + palliLabiMoot > 800 || palliX < 0)
			palliXmuut *= -1;
		if (palliY < 0)
			palliYmuut *= -1;
		palliX += palliXmuut;
		palliY += palliYmuut;
	}

}
