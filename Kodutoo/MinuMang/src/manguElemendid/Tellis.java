package manguElemendid;

import java.awt.Color;
import java.awt.Graphics;

/** tellise klass*/
public class Tellis {

	/**tellise klassi muutujate kirjeldamine*/
	public static final int tellis_laius = 80;
	public static final int tellis_korgus = 25;

	public Color varv;
	private int telliseX, telliseY;
	private Type telliseTyyp;
	
	/** tellise t��pide kirjeldamine
	 * t��pide kirjeldamisel kasutatud Breakout Tutoriali abi
	 * http://www.java-forums.org/blogs/sunde887/93-breakout-tutorial.html*/
	public enum Type {
		IV (6, 700, Color.BLACK),
		III(3, 100, Color.RED),
		II(2, 50, Color.BLUE),
		I(1, 20, Color.GREEN),
		SURNUD(0, 0, Color.WHITE);
		private int elu;
		private Color varv;
		private int punktid;
		
		/** t��pide loomise konstruktor*/
		Type(int elu, int punktid, Color varv) {
			this.elu = elu;
			this.punktid = punktid;
			this.varv = varv; 
			}
		
		/**meetodid, mis tagastavad t��pide muutujaid*/
		
		public int tooPunktid() {
		return punktid;
		}

		public Color tooVarv() {
			return varv;
		}

		public int tooElu() {
			return elu;
		}
	}
	
	/** tellise loomise konstruktor*/
	public Tellis(int TelliseX, int TelliseY, Type TelliseTyyp) {
		telliseX = TelliseX;
		telliseY = TelliseY;
		telliseTyyp = TelliseTyyp;
		varv = Color.green;
	}

	/** meetodid, mis tagastavad tellise muutujaid*/
	public int tooTelliseX() {
		return telliseX;
	}

	public int tooTelliseY() {
		return telliseY;
	}
	
	/** meetod, mis tagastab tellise t��bi*/
	public Type tooTelliseTyyp() {
		return telliseTyyp;
	}

	/**loogika meetod, mis kontrollib, kas tellis ja pall on p�rkunud*/
	public boolean kokkuPorge(Pall b) {
		
		/** kontrollib, kas pall on tellise alumise v�i �lemise ��rega kohakuti */
		if (b.tooPalliX() <= (telliseX + tellis_laius) && b.tooPalliX() >= telliseX) {
			
			/**kontrollib, kas pall asub tellise alumise joone juures */
			if (b.tooPalliY() <= (telliseY + tellis_korgus) && b.tooPalliY() >= (telliseY + (tellis_korgus / 2))) {
				b.seaPalliYmuut(b.tooPalliYmuut() * -1);
				return true;
			}
			 /** kontrollib, kas pall asub tellise �lemise joone juures*/
			if (b.tooPalliY() + 20 <= telliseY + tellis_korgus / 2 && b.tooPalliY() + 20 >= (telliseY)) {
				b.seaPalliYmuut(b.tooPalliYmuut() * -1);
				return true;
			}
		}	/**kontrollib, kas pall on tellise k�lgedega kohakuti*/
		if (b.tooPalliY() <= telliseY + tellis_korgus&& b.tooPalliY() >= telliseY) {
			
			/** kontrollib, kas pall asub tellise vasaku k�lje juures*/
			if (b.tooPalliX() + 20 <= telliseX + tellis_laius/6 && b.tooPalliX() + 20 >= telliseX) {
				b.seaPalliXmuut(b.tooPalliXmuut() * -1);
				return true;
				
			} /**kontrollib, kas pall asub tellise parema k�lje juures*/
			if (b.tooPalliX() >= telliseX + tellis_laius / 6 && b.tooPalliX() <= telliseX + tellis_laius) {
				b.seaPalliXmuut(b.tooPalliXmuut() * -1);
				return true;
			}
		}
		return false;
	}

	/**tellise joonistamise meetod*/
	public void joonistaTellis(Graphics g) {
		g.setColor(Color.GRAY);
		g.fillRect(telliseX, telliseY, tellis_laius, tellis_korgus);
		g.setColor(telliseTyyp.varv);
		g.fillRect((telliseX + 2), (telliseY + 2), tellis_laius - 4, tellis_korgus - 4);
		g.setColor(Color.black);
		g.drawRect((telliseX + 2), (telliseY + 2), tellis_laius - 4, tellis_korgus - 4);

	}
	
	/**meetod, mis kirjeldab, mis juhtub tellise selle elude arvu muutumise korral
	 * meetodi loomisel kasutatud Breakout tutoriali abi
	 * http://www.java-forums.org/blogs/sunde887/93-breakout-tutorial.html*/
	public void alamTyyp() {
		switch (telliseTyyp.elu) {
		case 4:
			telliseTyyp = Type.III;
			break;
		case 3:
			telliseTyyp = Type.II;
			break;
		case 2:
			telliseTyyp = Type.I;
			break;
		case 1:
		default:
			telliseTyyp = Type.SURNUD;
			break;
		}
	}

	/**loogika meetod, mis kontrollib, kas tellisel on veel elusid
	 * meetodi loomisel kasutatud Breakout tutoriali abi
	 * http://www.java-forums.org/blogs/sunde887/93-breakout-tutorial.html*/
	public boolean dead() {
		if (telliseTyyp.elu == 0)
			return true;
		return false;
	}

}
